package omnidoc.business.domain.scan;

public enum ScheduleType {
    MANUAL, ONCE, DAILY, WEEKLY, CUSTOM
}
