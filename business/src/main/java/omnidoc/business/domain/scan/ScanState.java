package omnidoc.business.domain.scan;

public enum ScanState {
    PENDING, RUNNING, FINISHED, ABORTED
}
