package omnidoc.business.domain.users;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

@NamedQueries({
        @NamedQuery(
                name = User.findByUsername,
                query = "SELECT u FROM User u WHERE u.username = :username"
        )
})

@Entity
public class User {

    public static final String findByUsername = "User.findByUsername";

    @Id
    private String username;
    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
