package omnidoc.business.domain.topology;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class DynamicAttribute {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Attribute> values;

    public DynamicAttribute() {
    }

    public DynamicAttribute(String name) {
        this.name = name;
        values = new ArrayList<>();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Attribute> getValues() {
        return values;
    }

    public void setValues(List<Attribute> values) {
        this.values = values;
    }

    public void addValue(String value) {
        this.values.add(new Attribute(value));
    }
}