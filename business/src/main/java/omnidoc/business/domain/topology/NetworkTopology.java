package omnidoc.business.domain.topology;

import javax.persistence.*;
import java.util.Set;

// TODO: Adapt Architecture for Lazy Loading

@Entity
public class NetworkTopology {

    public static final String findAll = "NetworkTopology.findAll";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<NetworkComponent> components;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<NetworkConnection> connections;

    public NetworkTopology() {
    }

    public NetworkTopology(Set<NetworkComponent> components, Set<NetworkConnection> connections) {
        this.components = components;
        this.connections = connections;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Set<NetworkComponent> getComponents() {
        return components;
    }

    public void setComponents(Set<NetworkComponent> components) {
        this.components = components;
    }

    public Set<NetworkConnection> getConnections() {
        return connections;
    }

    public void setConnections(Set<NetworkConnection> connections) {
        this.connections = connections;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (!(obj instanceof NetworkTopology)) return false;
        NetworkTopology topology = (NetworkTopology) obj;
        return id != null ? id.equals(topology.getId()) :
                (components == null || components.equals(topology.getComponents())) &&
                        (connections == null || connections.equals(topology.getConnections()));
    }

    @Override
    public int hashCode() {
        int hash = 53;
        return 7 * hash + (id != null ? id.hashCode() : 0) +
                7 * hash + (components != null ? components.hashCode() : 0) +
                7 * hash + (connections != null ? connections.hashCode() : 0);
    }
}
