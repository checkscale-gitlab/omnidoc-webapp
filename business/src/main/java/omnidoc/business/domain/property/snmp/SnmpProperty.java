package omnidoc.business.domain.property.snmp;

import omnidoc.business.domain.property.NetworkProperty;
import omnidoc.business.util.RegexUtil;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.Set;

@Entity
public class SnmpProperty extends NetworkProperty {

    @Enumerated(EnumType.STRING)
    private SNMP_VERSION version;

    @NotNull(message = "{property.oid.null}")
    @ManyToMany(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
    private Set<ObjectIdentifier> objectIdentifiers;

    @NotNull(message = "{snmpCommunityProperty.snmpString.null}")
    @Pattern(regexp = RegexUtil.NO_SPACES_PATTERN, message = "{communityString.invalid}")
    private String communityString = "public";

    public SnmpProperty() {
    }

    public SnmpProperty(String ip, int subnet, Set<ObjectIdentifier> objectIdentifiers, SNMP_VERSION version, String communityString) {
        super(ip, subnet);
        this.version = version;
        this.objectIdentifiers = objectIdentifiers;
        this.communityString = communityString;
    }

    public SNMP_VERSION getVersion() {
        return version;
    }

    public void setVersion(SNMP_VERSION version) {
        this.version = version;
    }

    public Set<ObjectIdentifier> getObjectIdentifiers() {
        return objectIdentifiers;
    }

    public void setObjectIdentifiers(Set<ObjectIdentifier> objectIdentifiers) {
        this.objectIdentifiers = objectIdentifiers;
    }

    public String getCommunityString() {
        return communityString;
    }

    public void setCommunityString(String communityString) {
        this.communityString = communityString;
    }
}
