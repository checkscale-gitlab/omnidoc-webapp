package omnidoc.business.domain.property.snmp;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;
import java.util.Set;

@Entity
public class SnmpCredentialProperty extends SnmpProperty {

    @NotNull(message = "{snmpCredentialProperty.snmpUsername.null}")
    private String username;

    @NotNull(message = "{snmpCredentialProperty.snmpPassword.null}")
    private String password;

    private String privateKey;

    @Enumerated(EnumType.STRING)
    private SECURITY_LEVEL securityLevel;

    @Enumerated(EnumType.STRING)
    private HASH_ALGORITHM hashAlgorithm;

    @Enumerated(EnumType.STRING)
    private ENCRYPTION_ALGORITHM encryptionAlgorithm;

    public SnmpCredentialProperty() {
    }

    public SnmpCredentialProperty(String ip, int subnetBits, Set<ObjectIdentifier> objectIdentifiers, String communityScan, String username, String snmpPassword) {
        super(ip, subnetBits, objectIdentifiers, SNMP_VERSION.VERSION_3, communityScan);
        this.securityLevel = SECURITY_LEVEL.NO_AUTH_NO_PRIV;
        this.username = username;
        this.password = snmpPassword;
    }

    public SnmpCredentialProperty(String ip, int subnetBits, Set<ObjectIdentifier> objectIdentifiers, String communityScan, String username, String snmpPassword, HASH_ALGORITHM hashAlgorithm) {
        super(ip, subnetBits, objectIdentifiers, SNMP_VERSION.VERSION_3, communityScan);
        this.securityLevel = SECURITY_LEVEL.AUTH_NO_PRIV;
        this.username = username;
        this.password = snmpPassword;
        this.hashAlgorithm = hashAlgorithm;
    }

    public SnmpCredentialProperty(String ip, int subnetBits, Set<ObjectIdentifier> objectIdentifiers, String communityScan, String username, String snmpPassword, HASH_ALGORITHM hashAlgorithm, ENCRYPTION_ALGORITHM encryptionAlgorithm, String privateKey) {
        super(ip, subnetBits, objectIdentifiers, SNMP_VERSION.VERSION_3, communityScan);
        this.securityLevel = SECURITY_LEVEL.AUTH_PRIV;
        this.username = username;
        this.password = snmpPassword;
        this.hashAlgorithm = hashAlgorithm;
        this.encryptionAlgorithm = encryptionAlgorithm;
        this.privateKey = privateKey;
    }


    public String getUsername() {
        return username;
    }

    public void setUsername(String snmpUsername) {
        this.username = snmpUsername;
    }

    public String getPrivateKey() {
        return privateKey;
    }

    public void setPrivateKey(String privateKey) {
        this.privateKey = privateKey;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String snmpPassword) {
        this.password = snmpPassword;
    }

    public SECURITY_LEVEL getSecurityLevel() {
        return securityLevel;
    }

    public void setSecurityLevel(SECURITY_LEVEL securityLevel) {
        this.securityLevel = securityLevel;
    }

    public HASH_ALGORITHM getHashAlgorithm() {
        return hashAlgorithm;
    }

    public void setHashAlgorithm(HASH_ALGORITHM hashAlgorithm) {
        this.hashAlgorithm = hashAlgorithm;
    }

    public ENCRYPTION_ALGORITHM getEncryptionAlgorithm() {
        return encryptionAlgorithm;
    }

    public void setEncryptionAlgorithm(ENCRYPTION_ALGORITHM encryptionAlgorithm) {
        this.encryptionAlgorithm = encryptionAlgorithm;
    }
}
