package omnidoc.business.domain.property.snmp;

public enum SNMP_VERSION {
    VERSION_1, VERSION_2, VERSION_3
}
