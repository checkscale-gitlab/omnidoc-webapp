package omnidoc.business.domain.property.snmp;

import javax.persistence.*;

@NamedQueries({
        @NamedQuery(
                name = ObjectIdentifier.findAll,
                query = "SELECT o FROM ObjectIdentifier o"
        ),
        @NamedQuery(
                name = ObjectIdentifier.findByOid,
                query = "SELECT o FROM ObjectIdentifier o WHERE o.oid = :oid"
        ),
        @NamedQuery(
                name = ObjectIdentifier.findByName,
                query = "SELECT o FROM ObjectIdentifier o WHERE o.name = :name"
        )
})

@Entity
public class ObjectIdentifier {

    public static final String
            findAll = "ObjectIdentifier.findAll",
            findByOid = "ObjectIdentifier.findByOid",
            findByName = "ObjectIdentifier.findByName";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name, oid;

    public ObjectIdentifier() {
    }

    public ObjectIdentifier(String name, String oid) {
        this.name = name;
        this.oid = oid;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (!(obj instanceof ObjectIdentifier)) return false;
        ObjectIdentifier scan = (ObjectIdentifier) obj;
        return id != null ? id.equals(scan.getId()) :
                (name == null || name.equals(scan.getName())) &&
                        (oid == null || oid.equals(scan.getName()));
    }

    @Override
    public int hashCode() {
        int hash = 53;
        return 7 * hash + (name != null ? name.hashCode() : 0) +
                7 * hash + (oid != null ? oid.hashCode() : 0);
    }

}
