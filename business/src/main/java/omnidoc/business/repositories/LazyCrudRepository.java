package omnidoc.business.repositories;

import java.util.List;

public interface LazyCrudRepository<T, ID> {

    List<T> findAll();

    T findById(ID id);

    T save(T t, boolean immediate);

    void update(T t);

    void remove(T t);

}
