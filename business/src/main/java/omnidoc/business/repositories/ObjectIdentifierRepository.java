package omnidoc.business.repositories;

import omnidoc.business.domain.property.snmp.ObjectIdentifier;

public interface ObjectIdentifierRepository extends CrudRepository<ObjectIdentifier, Long> {
    ObjectIdentifier getByOid(String objectIdentifier);

    ObjectIdentifier getByName(String name);
}
