package omnidoc.business.repositories;

import omnidoc.business.domain.site.NetworkSite;

public interface SiteRepository extends LazyCrudRepository<NetworkSite, Long> {
}
