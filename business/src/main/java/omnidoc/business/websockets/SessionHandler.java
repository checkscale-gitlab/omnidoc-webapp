package omnidoc.business.websockets;

import javax.enterprise.context.ApplicationScoped;
import javax.websocket.Session;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

@ApplicationScoped
public class SessionHandler {

    private static final String MESSAGE_UPDATE_SCAN = "update";

    private final Set<Session> sessions = new HashSet<>();

    void addSession(Session session) {
        sessions.add(session);
    }

    void removeSession(Session session) {
        sessions.remove(session);
    }

    public void sendUpdateScanMessage() {
        sessions.forEach(session -> {
            try {
                session.getBasicRemote().sendText(MESSAGE_UPDATE_SCAN);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }
}