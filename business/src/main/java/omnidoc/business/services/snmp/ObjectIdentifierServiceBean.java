package omnidoc.business.services.snmp;

import omnidoc.business.domain.property.snmp.ObjectIdentifier;
import omnidoc.business.repositories.ObjectIdentifierRepository;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Stateless
public class ObjectIdentifierServiceBean implements ObjectIdentifierService {

    @Inject
    private ObjectIdentifierRepository oidRepository;

    @Override
    public Set<ObjectIdentifier> getDefaultObjectIdentifier() {
        return new HashSet<>(Arrays.asList(
                getOrCreateOid("MODEL", ".1.3.6.1.2.1.47.1.1.1.1.13.1001"),
                getOrCreateOid("SERIALNUMBER", ".1.3.6.1.2.1.47.1.1.1.1.11.1001"),
                getOrCreateOid("HOSTNAME", ".1.3.6.1.2.1.1.5.0"),
                getOrCreateOid("MAC", ".1.3.6.1.2.1.2.2.1.6"),
                getOrCreateOid("VLAN", ".1.3.6.1.2.1.4.20.1.2"),
                getOrCreateOid("IMAGE", ".1.3.6.1.2.1.47.1.1.1.1.9")
        ));
    }

    @Override
    public List<ObjectIdentifier> findAll() {
        return oidRepository.findAll();
    }

    @Override
    public ObjectIdentifier findById(Long id) {
        return oidRepository.findById(id);
    }

    @Override
    public ObjectIdentifier findByObjectIdentifier(String objectIdentifier) {
        return oidRepository.getByOid(objectIdentifier);
    }

    @Override
    public void add(ObjectIdentifier oid) {
        oidRepository.save(oid);
    }

    @Override
    public void update(ObjectIdentifier oid) {
        oidRepository.update(oid);
    }

    @Override
    public void remove(ObjectIdentifier oid) {
        oidRepository.remove(oid);
    }

    private ObjectIdentifier getOrCreateOid(String name, String oid) {
        ObjectIdentifier o = oidRepository.getByName(name);
        return o != null ? o : oidRepository.save(new ObjectIdentifier(name, oid));
    }

}
