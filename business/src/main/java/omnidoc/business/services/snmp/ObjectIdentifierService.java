package omnidoc.business.services.snmp;

import omnidoc.business.domain.property.snmp.ObjectIdentifier;

import java.util.List;
import java.util.Set;

public interface ObjectIdentifierService {

    Set<ObjectIdentifier> getDefaultObjectIdentifier();

    List<ObjectIdentifier> findAll();

    ObjectIdentifier findById(Long id);

    ObjectIdentifier findByObjectIdentifier(String objectIdentifier);

    void add(ObjectIdentifier oid);

    void update(ObjectIdentifier oid);

    void remove(ObjectIdentifier oid);

}
