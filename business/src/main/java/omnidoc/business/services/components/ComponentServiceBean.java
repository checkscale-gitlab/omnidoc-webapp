package omnidoc.business.services.components;

import omnidoc.business.domain.topology.NetworkComponent;
import omnidoc.business.repositories.ComponentRepository;

import javax.ejb.Stateless;
import javax.inject.Inject;

@Stateless
public class ComponentServiceBean implements ComponentService {

    @Inject
    private ComponentRepository componentRepository;

    @Override
    public NetworkComponent findById(Long id) {
        return componentRepository.findById(id);
    }

    @Override
    public void save(NetworkComponent component) {
        componentRepository.save(component);
    }

    @Override
    public void update(NetworkComponent component) {
        componentRepository.update(component);
    }

    @Override
    public void remove(NetworkComponent component) {
        componentRepository.remove(component);
    }
}
