package omnidoc.business.services.components;

import omnidoc.business.domain.topology.NetworkComponent;

public interface ComponentService {

    NetworkComponent findById(Long id);

    void save(NetworkComponent component);

    void update(NetworkComponent component);

    void remove(NetworkComponent component);

}

