package omnidoc.business.services.settings;

import omnidoc.business.domain.site.NetworkSite;
import omnidoc.business.repositories.SettingsRepository;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

@Stateless
public class SettingsServiceBean implements ISettingsService {

    @Inject
    private SettingsRepository settingsRepository;

    @Override
    public void addPreferences(NetworkSite site, List<String> panelNames) {
        panelNames.forEach(name -> addPreference(site, name));
    }

    @Override
    public void addPreference(NetworkSite site, String panelName) {
        settingsRepository.addPreference(site.getId(), panelName);
    }

    @Override
    public List<String> findAllPanelsBySite(NetworkSite site) {
        if (site == null) return new ArrayList<>();
        return settingsRepository.getPanelNamesBySiteId(site.getId());
    }

    @Override
    public void removePreference(NetworkSite site, String panelName) {
        settingsRepository.removePreference(site.getId(), panelName);
    }
}
