package omnidoc.business.services.settings;

import omnidoc.business.domain.site.NetworkSite;

import java.util.List;

public interface ISettingsService {

    List<String> findAllPanelsBySite(NetworkSite site);

    void addPreferences(NetworkSite site, List<String> preferences);

    void addPreference(NetworkSite site, String preference);

    void removePreference(NetworkSite site, String preference);
}
