package omnidoc.business.util;

import omnidoc.business.domain.property.snmp.ObjectIdentifier;
import omnidoc.business.domain.topology.ComponentType;
import omnidoc.business.domain.topology.DynamicAttribute;
import omnidoc.business.domain.topology.NetworkComponent;
import omnidoc.business.domain.topology.NetworkTopology;

import javax.json.*;
import java.io.InputStream;
import java.io.Reader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static omnidoc.business.domain.scan.ScanConstants.*;

public class NetworkTopologyParser {

    private static final String NO_SNMP_SUPPORT = "noSNMPsupport";

    private Set<ObjectIdentifier> objectIdentifiers;
    private JsonReader reader;
    private boolean errorFlag;


    public NetworkTopologyParser(Set<ObjectIdentifier> objectIdentifiers, Reader reader) {
        this.objectIdentifiers = objectIdentifiers;
        this.reader = Json.createReader(reader);
    }

    public NetworkTopologyParser(Set<ObjectIdentifier> objectIdentifiers, InputStream inputStream) {
        this.objectIdentifiers = objectIdentifiers;
        this.reader = Json.createReader(inputStream);
    }

    public NetworkTopology parseJson() throws Exception {
        NetworkTopology topology = new NetworkTopology();
        Set<NetworkComponent> components = new HashSet<>();

        JsonArray jsonArray = getComponentArray();

        if (jsonArray == null || jsonArray.isEmpty())
            throw new Exception("Couldnt read JSON");

        for (int i = 0; i < jsonArray.size(); i++) {
            JsonObject json = jsonArray.getJsonObject(i);

            JsonValue ip = get(json, IP_ADDRESS);
            JsonValue serialnumber = get(json, SERIALNUMBER);
            JsonValue macAdresses = get(json, MAC);
            JsonValue modelName = get(json, MODEL_NAME);
            JsonValue hostname = get(json, HOSTNAME);

            removeStaticObjectIdentifiers();

            List<DynamicAttribute> dynamicAttributes = getDynamicProperties(json);

            NetworkComponent component = new NetworkComponent(
                    ComponentType.UNKOWN,
                    asString(ip),
                    asString(hostname),
                    asString(modelName),
                    asString(serialnumber),
                    asSet(macAdresses)
            );

            component.setDynamicAttributes(dynamicAttributes);

            components.add(component);
            errorFlag = false;
        }

        topology.setComponents(components);
        return topology;
    }

    /**
     * @param json JSON to be searched
     * @return List of Dynamic Attributes queried for
     */
    private List<DynamicAttribute> getDynamicProperties(JsonObject json) {
        List<DynamicAttribute> dynamicAttributes = new ArrayList<>();
        objectIdentifiers.forEach(oid -> {
            if (json.containsKey(oid.getName())) {
                JsonValue attribute = json.get(oid.getName());
                DynamicAttribute dynAttribute = new DynamicAttribute(oid.getName());

                if (isJsonString(attribute))
                    dynAttribute.addValue(asString(attribute));

                if (isJsonArray(attribute)) {
                    ((JsonArray) attribute).forEach(jsonValue -> {
                        if (isJsonString(jsonValue)) dynAttribute.addValue(asString(jsonValue));
                    });
                }

                dynamicAttributes.add(dynAttribute);
            }
        });
        return dynamicAttributes;
    }

    /**
     * This methods removes all objectIdentifiers which are not dynamic
     */
    private void removeStaticObjectIdentifiers() {
        objectIdentifiers.removeIf(oid -> oid.getName().equals(IP_ADDRESS));
        objectIdentifiers.removeIf(oid -> oid.getName().equals(SERIALNUMBER));
        objectIdentifiers.removeIf(oid -> oid.getName().equals(MAC));
        objectIdentifiers.removeIf(oid -> oid.getName().equals(MODEL_NAME));
        objectIdentifiers.removeIf(oid -> oid.getName().equals(HOSTNAME));
    }

    private JsonArray getComponentArray() {
        JsonObject root = reader.readObject();
        for (String subnet : root.keySet()) {
            return ((JsonArray) root.get(subnet));
        }
        return null;
    }

    private boolean isJsonString(JsonValue jsonValue) {
        return jsonValue.getValueType() == JsonValue.ValueType.STRING;
    }

    private boolean isJsonArray(JsonValue value) {
        return value.getValueType() == JsonValue.ValueType.ARRAY;
    }

    private String asString(JsonValue value) {
        if (value == null) return null;

        if (isJsonString(value)) {
            String s = ((JsonString) value).getString();
            if (!isErrorCode(s)) return s;
        }

        return null;
    }

    private Set<String> asSet(JsonValue value) {
        if (value == null || value.getValueType() != JsonValue.ValueType.ARRAY)
            return new HashSet<>();

        Set<String> set = new HashSet<>();
        ((JsonArray) value).forEach(o -> set.add(asString(o)));

        return set;
    }

    private JsonValue get(JsonObject object, String key) {
        if (key == null || !object.containsKey(key)) return null;

        try {
            String value = object.getString(key);
//            if (isErrorCode(value)) return null;
            isErrorCode(value);
        } catch (Exception ignored) {
        }

        return object.get(key);
    }

    private boolean isErrorCode(String error) {
        return error.equals(NO_SNMP_SUPPORT);
    }

}
