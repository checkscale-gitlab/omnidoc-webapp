package omnidoc.business.util;

public class ProcessMonitor<SUBJECT> implements Runnable {

    private final SUBJECT subject;
    private final Process process;
    private final ProcessResultListener<SUBJECT> listener;

    ProcessMonitor(SUBJECT subject, Process process, ProcessResultListener<SUBJECT> listener) {
        this.subject = subject;
        this.process = process;
        this.listener = listener;
    }

    @Override
    public void run() {
        listener.onProcessStarted(subject);
        try {
            int resultCode = process.waitFor();
            listener.onProcessTerminated(subject, resultCode);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
