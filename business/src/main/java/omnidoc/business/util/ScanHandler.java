package omnidoc.business.util;

import omnidoc.business.domain.property.snmp.SnmpCredentialProperty;
import omnidoc.business.domain.property.snmp.SnmpProperty;
import omnidoc.business.domain.scan.NetworkScan;

import javax.annotation.Resource;
import javax.ejb.Singleton;
import javax.enterprise.concurrent.ManagedExecutorService;
import javax.inject.Inject;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import static omnidoc.business.domain.scan.ScanConstants.*;

@Singleton
public class ScanHandler {

    private static final String
            scriptPathWindows = "C:\\Dev\\HTL-IMST\\OmniDoc\\omnidoc-webapp\\scripts\\windows\\script.bat",
            scriptPathLinux = "./snmpScan.bash";

    private String workingDir = null;

    @Resource
    private ManagedExecutorService executor;

    @Inject
    private ProcessResultListener<NetworkScan> listener;

    @Inject
    private Logger logger;

    public void setupScan(NetworkScan scan) throws ClassNotFoundException {
        String environment = determineEnvironment();
        logger.log(Level.INFO, "env.found", environment);

        List<String> scanParams = buildScanParameters(environment, scan);
        logger.log(Level.INFO, "script.params", String.join(" ", scanParams));

        startProcess(scan, scanParams);
    }

    private String determineEnvironment() {
        return System.getProperty("os.name");
    }

    private List<String> buildScanParameters(String environment, NetworkScan scan) throws ClassNotFoundException {
        return environment.equals("Linux") ?
                buildLinuxParameters(scan) : buildWindowsParameters(scan);
    }

    private List<String> buildWindowsParameters(NetworkScan scan) {
        return new ArrayList<>(Arrays.asList("cmd", "/c", "start", "/wait", scriptPathWindows, scan.getName()));
    }


    private List<String> buildLinuxParameters(NetworkScan scan) throws ClassNotFoundException {

        if (scan.getProperty() instanceof SnmpProperty) {
            SnmpProperty property = (SnmpProperty) scan.getProperty();
            return buildSnmpParameters(scan.getId(), property);
        }

        throw new TypeNotPresentException("SnmpProperty", new Throwable("Scan type not found"));
    }

    private List<String> buildSnmpParameters(Long scanId, SnmpProperty property) throws ClassNotFoundException {

        String version = "v";
        List<String> snmpParams = new ArrayList<>();
        switch (property.getVersion()) {
            case VERSION_1:
                version += "1";
                break;
            case VERSION_2:
                version += "2c";
                break;
            case VERSION_3:
                version += "3";
                SnmpCredentialProperty credential = (SnmpCredentialProperty) property;
                snmpParams.add(prepareParam(SNMP_USERNAME, credential.getUsername()));
                snmpParams.add(prepareParam(SNMP_PASSWORD, credential.getPassword()));
                snmpParams.add(prepareParam(SECURITY_LEVEL, credential.getSecurityLevel()));
                snmpParams.add(prepareParam(HASH_ALGORITHM, credential.getHashAlgorithm()));
                snmpParams.add(prepareParam(ENCRYPTION_ALGORITHM, credential.getEncryptionAlgorithm()));
                snmpParams.add(prepareParam(PRIVATE_KEY, credential.getPrivateKey()));
                break;
        }

        workingDir = "/scripts";
        if (!new File(workingDir + "/" + scriptPathLinux).exists())
            throw new ClassNotFoundException("script not found");

        List<String> params = new ArrayList<>(Arrays.asList(
                scriptPathLinux,
                prepareParam(JSON_ID, scanId),
                prepareParam(IP_ADDRESS, property.getIp()),
                prepareParam(SUBNET_MASK, property.getSubnetBits()),
                prepareParam(SNMP_VERSION, version),
                prepareParam(SNMP_COMMUNITY_STRING, property.getCommunityString())
        ));

        // Add Snmp Version specific Parameters
        params.addAll(snmpParams);

        // Adding variable Object Identifiers to parameters
        property.getObjectIdentifiers().forEach(
                o -> params.add(prepareParam(o.getName(), o.getOid()))
        );

        return params;
    }

    private void startProcess(NetworkScan scan, List<String> scanParams) {
        ProcessBuilder builder = new ProcessBuilder(scanParams);

        File executionFolder = new File(workingDir);
        if (executionFolder.exists()) builder.directory(executionFolder);

        builder.redirectErrorStream(true);
        try {
            Process process = builder.start();
            ProcessMonitor<NetworkScan> monitor = new ProcessMonitor<>(scan, process, listener);
            executor.execute(monitor);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * This prepares a parameter like: "key"="value"
     *
     * @param key   name of parameter
     * @param value value of parameter
     * @return prepared parameter
     */
    private String prepareParam(String key, Object value) {
        return key + "=" + value;
    }

}