package omnidoc.business.domain.scan.parsing;

import junit.framework.TestCase;
import omnidoc.business.domain.property.snmp.ObjectIdentifier;
import omnidoc.business.domain.topology.NetworkComponent;
import omnidoc.business.domain.topology.NetworkTopology;
import omnidoc.business.util.NetworkTopologyParser;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class NetworkScanResultParseTest extends TestCase {

    private InputStream stream;
    private Set<ObjectIdentifier> identifiers;

    private String JSON_TEXT = "{\n" +
            "    \"10.0.10.0\": [\n" +
            "        {\n" +
            "            \"IP\": \"10.0.10.253\",\n" +
            "            \"SUBNET\": \"24\",\n" +
            "            \"MODELNAME\": \"noSNMPsupport\",\n" +
            "            \"SERIALNUMBER\": \"noSNMPsupport\",\n" +
            "            \"MAC\": \"noSNMPsupport\",\n" +
            "            \"HOSTNAME\": \"noSNMPsupport\",\n" +
            "            \"imageVersion\": \"noSNMPsupport\",\n" +
            "            \"vlans\": \"noSNMPsupport\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"IP\": \"10.0.10.254\",\n" +
            "            \"SUBNET\": \"24\",\n" +
            "            \"MODELNAME\": \"WS-C2960C-8PC-L\",\n" +
            "            \"SERIALNUMBER\": \"FOC1644Y4LK\",\n" +
            "            \"MAC\": [\n" +
            "                \"1CE6C7A3CEC0\",\n" +
            "                \"1CE6C7A3CEC1\",\n" +
            "                \"1CE6C7A3CEC2\",\n" +
            "                \"1CE6C7A3CE81\",\n" +
            "                \"1CE6C7A3CE82\",\n" +
            "                \"1CE6C7A3CE83\",\n" +
            "                \"1CE6C7A3CE84\",\n" +
            "                \"1CE6C7A3CE85\",\n" +
            "                \"1CE6C7A3CE86\",\n" +
            "                \"1CE6C7A3CE87\",\n" +
            "                \"1CE6C7A3CE88\",\n" +
            "                \"1CE6C7A3CE89\",\n" +
            "                \"1CE6C7A3CE8A\"\n" +
            "            ],\n" +
            "            \"HOSTNAME\": \"L2_Switch_8P.cisco.com\",\n" +
            "            \"imageVersion\": [\n" +
            "                \"12.2(55)EX3\"\n" +
            "            ],\n" +
            "            \"vlans\": [\n" +
            "                \"10\",\n" +
            "                \"20\"\n" +
            "            ]\n" +
            "        }\n" +
            "    ]\n" +
            "}";

    @Override
    public void setUp() throws Exception {
        super.setUp();

        stream = new ByteArrayInputStream(JSON_TEXT.getBytes());

        identifiers = new HashSet<>(Arrays.asList(
                new ObjectIdentifier("IP", "OIDIP"),
                new ObjectIdentifier("SUBNET", "OIDSUBNET"),
                new ObjectIdentifier("MODELNAME", "OIDMODELNAME"),
                new ObjectIdentifier("SERIALNUMBER", "OIDSERIALNUMBER"),
                new ObjectIdentifier("MAC", "OIDMAC"),
                new ObjectIdentifier("HOSTNAME", "OIDHOSTNAME"),
                new ObjectIdentifier("imageVersion", "OIDimageVersion"),
                new ObjectIdentifier("vlans", "OIDvlans")
        ));
    }

    public void testParseJsonFile() throws Exception {
        NetworkTopologyParser parser = new NetworkTopologyParser(identifiers, stream);
        NetworkTopology topology = parser.parseJson();

        assertNotNull(topology.getComponents());
        assertEquals(topology.getComponents().size(), 2);

        for (NetworkComponent component : topology.getComponents()) {
            System.out.println("Component: " + component);
        }
    }

}
