package omnidoc.business.domain.components;

import junit.framework.TestCase;
import omnidoc.business.domain.topology.ComponentType;
import omnidoc.business.domain.topology.NetworkComponent;

import java.util.Collections;

public class NetworkComponentHierarchyTest extends TestCase {

    private NetworkComponent routerComponent1,
            routerComponent2,
            switchComponent1,
            switchComponent2,
            clientComponent1,
            clientComponent2;

    @Override
    public void setUp() throws Exception {
        super.setUp();
        routerComponent1 = new NetworkComponent(ComponentType.ROUTER, "192.168.0.1", "router1", "Huawei AR3260", "123456789", Collections.singleton("FF:FF:FF:FF"));
        routerComponent2 = new NetworkComponent(ComponentType.ROUTER, "192.168.0.2", "router2", "Huawei AR2220", "123456789", Collections.singleton("FF:FF:FF:FF"));

        switchComponent1 = new NetworkComponent(ComponentType.SWITCH, "192.168.0.3", "switch1", "Cisco Catalyst 2950", "123456789", Collections.singleton("FF:FF:FF:FF"));
        switchComponent2 = new NetworkComponent(ComponentType.SWITCH, "192.168.0.4", "switch2", "Cisco Catalyst 2950", "123456789", Collections.singleton("FF:FF:FF:FF"));

        clientComponent1 = new NetworkComponent(ComponentType.CLIENT, "192.168.0.5", "client1", "Computer A", "123456789", Collections.singleton("FF:FF:FF:FF"));
        clientComponent2 = new NetworkComponent(ComponentType.CLIENT, "192.168.0.6", "client2", "Computer B", "123456789", Collections.singleton("FF:FF:FF:FF"));
    }

    public void testHierarchyIsSame() {
        assertEquals(0, routerComponent1.compareTo(routerComponent2));
        assertEquals(0, routerComponent2.compareTo(routerComponent1));

        assertEquals(0, switchComponent1.compareTo(switchComponent2));
        assertEquals(0, switchComponent2.compareTo(switchComponent1));

        assertEquals(0, clientComponent1.compareTo(clientComponent2));
        assertEquals(0, clientComponent2.compareTo(clientComponent1));
    }

    public void testHierarchyIsLess() {
        assertEquals(-1, switchComponent1.compareTo(routerComponent1));
        assertEquals(-1, clientComponent1.compareTo(routerComponent1));
        assertEquals(-1, clientComponent1.compareTo(switchComponent1));
    }

    public void testHierarchyIsGreater() {
        assertEquals(1, routerComponent1.compareTo(switchComponent1));
        assertEquals(1, routerComponent1.compareTo(clientComponent1));
        assertEquals(1, switchComponent1.compareTo(clientComponent1));
    }

}