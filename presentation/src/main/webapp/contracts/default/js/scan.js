$(document).ready(function () {
    setupWebSocket();
});

function setupWebSocket() {
    let webSocketUrl =
        "wss://" +
        window.location.host +
        "/webapp/scan";

    let webSocket = new WebSocket(webSocketUrl);

    webSocket.onopen = function () {
        console.log("WebSocket Open");
    };

    webSocket.onclose = function () {
        console.log("WebSocket Close");
    };

    webSocket.onerror = function (error) {
        console.log("WebSocket Error " + error);
    };

    webSocket.onmessage = function (message) {
        console.log("WebSocket Message");
        console.log(message);
        if (message.data === "updateScan") {
            updateScan();
        }
    };
}