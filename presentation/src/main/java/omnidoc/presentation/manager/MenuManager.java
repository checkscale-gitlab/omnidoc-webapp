package omnidoc.presentation.manager;

import omnidoc.presentation.model.panel.PanelType;
import omnidoc.presentation.util.Page;
import omnidoc.presentation.util.Resources;
import org.primefaces.PrimeFaces;
import org.primefaces.model.menu.DefaultMenuItem;
import org.primefaces.model.menu.DefaultMenuModel;
import org.primefaces.model.menu.DefaultSubMenu;
import org.primefaces.model.menu.MenuModel;

import javax.ejb.Singleton;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

@Singleton
public class MenuManager {

    private static final String ID_MENU_SIDE = "menu-side-ajax", ID_MENU_SMALL = "menu-small-ajax";
    private static final String CSS_SELECTED_MENU_ITEM = "menu-item-selected";

    private static MenuModel model;
    private static Map<String, DefaultMenuItem> menuMap = new HashMap<>();

    private static ResourceBundle bundle;

    private MenuManager() {
    }

    public static MenuModel getModel() {
        return model;
    }

    public static void setupMenuModel() {
        bundle = Resources.getMessageBundle(Resources.messagesBundle);

        model = new DefaultMenuModel();

        DefaultMenuItem startpageItem = createDefaultMenuItem(bundle.getString("menu.startpage"), "fa fa-home", Page.STARTPAGE);
        model.addElement(startpageItem);

        DefaultMenuItem dashboardItem = createDefaultMenuItem(bundle.getString("menu.dashboard"), "fa fa-tasks", Page.DASHBOARD);
        model.addElement(dashboardItem);

        DefaultMenuItem scanItem = createDefaultMenuItem(bundle.getString("menu.scan"), "fa fa-search", Page.SCAN);
        model.addElement(scanItem);

        DefaultMenuItem settingsItem = createDefaultMenuItem(bundle.getString("menu.settings"), "fa fa-cog", Page.SETTINGS);
        model.addElement(settingsItem);

        //Diagram submenu
        //createDiagramMenues();

        DefaultMenuItem logoutItem = new DefaultMenuItem(bundle.getString("menu.logoff"));
        logoutItem.setOnclick("PF('js-dlg-logout').show()");
        logoutItem.setIcon("fa fa-power-off");
        menuMap.put(Page.LOGOUT.getName(), logoutItem);
        model.addElement(logoutItem);
    }

    private static void createDiagramMenues() {
        DefaultSubMenu diagramSubMenu = new DefaultSubMenu("Diagrams");
        diagramSubMenu.setIcon("fa fa-sitemap");

        DefaultMenuItem simpleDiagram = createDefaultMenuItem(bundle.getString("diagrams." + PanelType.SIMPLE_DIAGRAM.getName()), "", Page.SIMPLE_DIAGRAM);
        diagramSubMenu.addElement(simpleDiagram);

        DefaultMenuItem organigram = createDefaultMenuItem(bundle.getString("diagrams." + PanelType.ORGANIGRAM.getName()), "", Page.ORGANIGRAM);
        diagramSubMenu.addElement(organigram);

        DefaultMenuItem mindmap = createDefaultMenuItem(bundle.getString("diagrams." + PanelType.MINDMAP.getName()), "", Page.MINDMAP);
        diagramSubMenu.addElement(mindmap);

        DefaultMenuItem editableDiagram = createDefaultMenuItem(bundle.getString("diagrams." + PanelType.EDITABLE_DIAGRAM.getName()), "", Page.EDITABLE_DIAGRAM);
        diagramSubMenu.addElement(editableDiagram);

        model.addElement(diagramSubMenu);
    }

    private static DefaultMenuItem createDefaultMenuItem(String menuTitle, String icon, Page page) {
        DefaultMenuItem menuItem = new DefaultMenuItem(menuTitle);
        menuItem.setOutcome(NavigationManager.to(page));
        menuItem.setIcon(icon);
        menuMap.put(page.getName(), menuItem);
        return menuItem;
    }

    static void enableSiteMenuEntries() {
        menuMap.get(Page.DASHBOARD.getName()).setDisabled(false);
        menuMap.get(Page.SCAN.getName()).setDisabled(false);
        updateMenus();
    }

    static void disableSiteMenuEntries() {
        menuMap.get(Page.DASHBOARD.getName()).setDisabled(true);
        menuMap.get(Page.SCAN.getName()).setDisabled(true);
        updateMenus();
    }

    private static void updateMenus() {
        PrimeFaces.current().ajax().update(ID_MENU_SIDE, ID_MENU_SMALL);
    }

    public static void setMenuItemSelected(String currentPageName) {
        for (String pageName : menuMap.keySet()) {
            menuMap.get(pageName).setStyleClass(currentPageName.equals(pageName) ? CSS_SELECTED_MENU_ITEM : null);
        }
    }
}
