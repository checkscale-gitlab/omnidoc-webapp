package omnidoc.presentation.converter;

import omnidoc.business.domain.property.snmp.ObjectIdentifier;
import omnidoc.business.services.snmp.ObjectIdentifierService;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;

@Named
public class ObjectIdentifierSelectConverter implements Converter {

    @Inject
    private ObjectIdentifierService oidService;

    @Override
    public Object getAsObject(FacesContext facesContext, UIComponent uiComponent, String objectIdentifierId) {
        if (objectIdentifierId == null || objectIdentifierId.isEmpty()) return null;
        return oidService.findById(Long.parseLong(objectIdentifierId));
    }

    @Override
    public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object value) {
        return value == null ? null : ((ObjectIdentifier) value).getId().toString();
    }

}
