package omnidoc.presentation.model.panel;

import java.util.List;

public class DashboardPanel {
    private String id, header;
    private List<PanelAction> actions;
    private PanelType type;

    public DashboardPanel(String id, String header, PanelType type) {
        this.id = id;
        this.header = header;
        this.type = type;
    }

    public DashboardPanel(String id, String header, PanelType type, List<PanelAction> actions) {
        this.id = id;
        this.header = header;
        this.actions = actions;
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public String getHeader() {
        return header;
    }

    public String getIncludePath() {
        return type.getPath();
    }

    public List<PanelAction> getActions() {
        return actions;
    }

    public PanelType getType() {
        return type;
    }

    public void setType(PanelType type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof DashboardPanel) {
            return this.id.equals(((DashboardPanel) obj).getId());
        }
        return false;
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }
}
