package omnidoc.presentation.model.panel;

import omnidoc.presentation.util.Page;
import omnidoc.presentation.util.Resources;

import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class PanelProducer {

    private static int panelCounter = 0;

    public static DashboardPanel fromType(PanelType panelType) {
        if (panelType == null) return null;
        String id = panelType.getName() + "-panel" + ++panelCounter;
        ResourceBundle bundle = Resources.getMessageBundle(Resources.dashboardBundle);
        String messageKey = panelType.getName();
        String header;
        if (bundle.containsKey(messageKey)) {
            header = bundle.getString(messageKey);
        } else {
            header = bundle.getString("panel.default");
        }

        List<PanelAction> actionList = new ArrayList<>();
        for (Page page : Page.values()) {
            if (page.getName().equals(panelType.getName())) actionList.add(new ZoomAction(page));
        }

        // TODO: How to handle Ids when multiple panels are in the dashboard?
        if (panelType == PanelType.COMPONENTS) actionList.add(new ExportAction("PF('js-dlg-export').show()"));

        return new DashboardPanel(id, header, panelType, actionList);
    }

    public static DashboardPanel fromName(String panelName) {
        PanelType panelType = PanelType.fromName(panelName);
        if (panelType == null) return null;
        return fromType(panelType);
    }

}
