package omnidoc.presentation.util;

import javax.enterprise.context.Dependent;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Produces;
import javax.faces.context.FacesContext;
import java.util.Locale;
import java.util.ResourceBundle;

@Dependent
public class Resources {
    public static final String messagesBundle = "messages",
            startpageBundle = "startpage",
            dashboardBundle = "dashboard",
            scanBundle = "scan";


    public static ResourceBundle getMessageBundle(String bundleName) {
        Locale locale = FacesContext.getCurrentInstance().getViewRoot().getLocale();
        return ResourceBundle.getBundle(bundleName, locale);
    }

    @Produces
    @RequestScoped
    public FacesContext produceFacesContext() {
        return FacesContext.getCurrentInstance();
    }

    public static class Colors {

        public static final String
                primaryBlue = "122034",
                accentYellow = "FFB71C",
                accentYellowDark = "FF9012";

    }

    public static class Images {

        private static final String
                networkPath = "network/",
                routerPath = "router/",
                switchPath = "switch/",
                workstationPath = "workstation/";

        public static final String
                ROUTER_SMALL = networkPath + routerPath + "router-50.png",
                ROUTER_BIG = networkPath + routerPath + "router-100.png",
                SWITCH_SMALL = networkPath + switchPath + "switch-48.png",
                SWITCH_BIG = networkPath + switchPath + "switch-96.png",
                WORKSTATION_SMALL = networkPath + workstationPath + "workstation-50.png",
                WORKSTATION_BIG = networkPath + workstationPath + "workstation-100.png";

    }
}
