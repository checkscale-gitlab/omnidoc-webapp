package omnidoc.presentation.util;

public enum Page {
    STARTPAGE("startpage"),
    DASHBOARD("dashboard"),
    SCAN("scan"),
    DETAIL("detail"),
    SETTINGS("settings"),

    SIMPLE_DIAGRAM("simple-diagram"),
    EDITABLE_DIAGRAM("editable-diagram"),
    MINDMAP("mindmap"),
    ORGANIGRAM("organigram"),

    LOGIN("login"),
    LOGOUT(LOGIN.getName());

    private String name;

    Page(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

}
