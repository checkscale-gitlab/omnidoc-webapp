package omnidoc.presentation.util;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import java.io.Serializable;

@Named
@SessionScoped
public class TemplateBean implements Serializable {
    private static final long serialVersionUID = -3588760622037176609L;
    private String contractName = "default";

    public String getContract() {
        return contractName;
    }

    public void setContract(String contractName) {
        this.contractName = contractName;
    }
}
