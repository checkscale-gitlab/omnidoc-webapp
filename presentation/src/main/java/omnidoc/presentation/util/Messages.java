package omnidoc.presentation.util;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import java.text.MessageFormat;

@Named
@RequestScoped
public class Messages {

    public String format(String message, String parameter) {
        return MessageFormat.format(message, parameter);
    }

}
