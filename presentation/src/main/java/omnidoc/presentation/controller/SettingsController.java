package omnidoc.presentation.controller;

import omnidoc.presentation.data.SiteProducer;
import omnidoc.presentation.manager.NavigationManager;
import omnidoc.presentation.util.Page;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;

// TODO: Add User editing page
/* TODO: Add more Roles to application
 * Role Administrator: should be able to edit everything including users
 * Role User: should be allowed to edit his own Sites
 */

@Named
@ViewScoped
public class SettingsController implements Serializable {

    private static final long serialVersionUID = 4375338974139778106L;

    @Inject
    private SiteProducer siteProducer;

    public String addExampleSite() {
        siteProducer.createExampleSite();
        return NavigationManager.to(Page.STARTPAGE);
    }

    public String addSiteWithManualScans() {
        siteProducer.createManualTestScans();
        return NavigationManager.to(Page.STARTPAGE);
    }

    public String addSiteForPresentation() {
        siteProducer.createSiteForPresentation();
        return NavigationManager.to(Page.STARTPAGE);
    }

    public String addSiteForPresentationFinal() {
        siteProducer.createSiteForPresentationFinal();
        return NavigationManager.to(Page.STARTPAGE);
    }
}
