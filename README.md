# Omnidoc Webapplication

## Prerequisites
For easy deployment, **docker** and **docker-compose** has to be installed.
Otherwise, a wildfly and mysql server is needed.

## Run the application

1. Run the `./build.sh` script. <br />
   This script simply packages the program with maven and deploys needed containers with `docker-compose` as defined in the `docker-compose.yml` file.

2. Access the application under `https://localhost:8443/webapp/`

3. Login with the username 'admin' and the password '123'.

4. Shutdown containers with `./destroy.sh`
