#!/bin/bash

# Ziel dieses Skripte ist es, herauszufinden welche Hosts erreichbar sind
# Ein großes Netz mit > 255 Hosts wird weitere Bereiche von 0 bis 255 aufgeteilt
# dabei wird der Broadcast und NetzID nicht angesprochen (Firewall verdächtigt nichts)


function startPingScan {


##### erster Host
ersterHost=$1							#z.B. 192.168.0.1

e_ipBer1=${ersterHost:0:`expr index "$ersterHost" .`-1}		#192		=> Beispiel mit der IP: 192.168.0.0
mitte01=${ersterHost:`expr index "$ersterHost" .`}		#168.0.0
e_ipBer2=${mitte01:0:`expr index "$mitte01" .`-1}		#168
mitte02=${mitte01:`expr index "$mitte01" .`}			#0.0
e_ipBer3=${mitte02:0:`expr index "$mitte02" .`-1}		#0
e_ipBer4=${mitte02:`expr index "$mitte02" .`}			#0
#####

##### letzter Host
letzterHost=$2							#z.B. 192.168.0.254

l_ipBer1=${letzterHost:0:`expr index "$letzterHost" .`-1}	#192		=> Beispiel mit der IP: 192.168.0.0
mitte01=${letzterHost:`expr index "$letzterHost" .`}		#168.0.0
l_ipBer2=${mitte01:0:`expr index "$mitte01" .`-1}		#168
mitte02=${mitte01:`expr index "$mitte01" .`}			#0.0
l_ipBer3=${mitte02:0:`expr index "$mitte02" .`-1}		#0
l_ipBer4=${mitte02:`expr index "$mitte02" .`}			#0
####



validateIfNetzid="true"
scriptFertig="false"
while [ true ]
do
if [ $e_ipBer2 -eq $l_ipBer2 ]					#Wenn dies true ist, ist entweder der Bereich fertig oder er war schon im vorhinein fertig weil das Netz nicht größer als SM /16 ist
then
	if [ $e_ipBer3 -eq $l_ipBer3 ]				#Wenn dies true ist, ist entweder der Bereich fertig oder er war schon im vorhinein fertig weil das Netz nicht größer als SM /24 ist
	then

		if [ "$scriptFertig" == "true" ]
		then
			break					#fertig im Skript
		else
			netzID="$e_ipBer1"".""$e_ipBer2"".""$e_ipBer3""."
			ersterHost="$e_ipBer4"
			letzterHost="$l_ipBer4"
			#echo "$netzID""_""$ersterHost""_""$letzterHost" >>/root/Desktop/bashSkripte/ausgabe.txt
			runPing $netzID $ersterHost $letzterHost $3	
			break					#fertig im Skript
		fi
		
		else
		while [ true ]
		do
			if [ $e_ipBer3 -eq $l_ipBer3 ]		#Wenn dies true ist, ist der Bereich fertig; die SM muss zwischen /16 und /24 liegen
			then
			netzID="$e_ipBer1"".""$e_ipBer2"".""$e_ipBer3""."
			ersterHost=0
			letzterHost=254
			#echo "$netzID""_""$ersterHost""_""$letzterHost" >>/root/Desktop/bashSkripte/ausgabe.txt
			runPing $netzID $ersterHost $letzterHost $3 #dies wird nicht in den Hintergrund verschoben da dies der letzte Bereich ist und danach die SNMP Scans ausgeführt werden müssen
			sleep 5					#damit wirklich alle fertig geworden sind
			scriptFertig="true"			#Zeile 88
			break
			else
			netzID="$e_ipBer1"".""$e_ipBer2"".""$e_ipBer3""."
			if [ "$validateIfNetzid" == "true" ]
			then
				ersterHost=1
				letzterHost=255
				validateIfNetzid="false"
			else
				ersterHost=0
				letzterHost=255
			fi
			#echo "$netzID""_""$ersterHost""_""$letzterHost" >>/root/Desktop/bashSkripte/ausgabe.txt
			runPing $netzID $ersterHost $letzterHost $3 & #Durch das & Zeichen wird der Aufruf in den Hintergrund verschoben und das Skript kann weiter machen
			e_ipBer3=$(($e_ipBer3+1))
			fi
		done
	fi
else
		while [ true ]
		do
			if [ $e_ipBer2 -eq $l_ipBer2 ]		#Wenn dies true ist, ist der Bereich fertig; die SM muss zwischen /8 und /16 liegen
			then	
			netzID="$e_ipBer1"".""$e_ipBer2"".""$e_ipBer3""."
			ersterHost=0
			letzterHost=255
			#echo "$netzID""_""$ersterHost""_""$letzterHost" >>/root/Desktop/bashSkripte/ausgabe.txt
			runPing $netzID $ersterHost $letzterHost $3 &
			e_ipBer3=$(($e_ipBer3+1))
			break
			else
			netzID="$e_ipBer1"".""$e_ipBer2"".""$e_ipBer3""."
			if [ "$validateIfNetzid" == "true" ]
			then
				ersterHost=1
				letzterHost=255
				validateIfNetzid="false"
			else
				ersterHost=0
				letzterHost=255
			fi
			#echo "$netzID""_""$ersterHost""_""$letzterHost" >>/root/Desktop/bashSkripte/ausgabe.txt
			runPing $netzID $ersterHost $letzterHost $3 &
				if [ $e_ipBer3 -eq $l_ipBer3 ]	#Wenn dies true ist, ist der IP Bereich 3 voll und muss wieder auf null zurückgesetzt werden; IP Bereich 2 wird eines hochgezählt
				then
				e_ipBer3=0
				e_ipBer2=$(($e_ipBer2+1))
				else
				e_ipBer3=$(($e_ipBer3+1))
				fi
			fi			
		done
fi
done
}


function runPing {
netzID=$1
firstHost=$2
lastHost=$3
for i in $(eval echo "$netzID{$firstHost..$lastHost}")
do
#echo $i $4
nice -n 19 ping -c 1 -w 1  "$i" >/dev/null 2>&1 &&
echo $i >> successful_ping$4.txt 2>&1				#$4 ist die Scan ID (damit andere Scans sich nicht untereinander stören)
# -c 1 ... wie viele Pings gesendet werden
# -w 1 ... die maximale Dauer eines Pinges in Sekunden
# >/dev/null ... sendet die Ausgabe ins leere
# 2>&1 ... sendet eine mögliche Fehlermeldung auch ins leere
# && ... wenn der Ping erfolgreich war wird die nachfolgende Zeile aufgerufen
#tcp ping nmap port 80
#nice level prio nachschauen um die jobs mit einer nidrigen prio auszuführen
#floss lizenz von snmpwalk => opn source
done
}


