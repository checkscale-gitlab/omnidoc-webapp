#!/bin/bash
source startEndeIPBereich.bash
source pingScan.bash
# in diesem Skript werden die SNMP Abfragen gemacht
# es wird unteranderem entschieden ob das IP Netz in mehrere Segmente aufgeteilt werden soll (dies geschieht in der "pingScan.bash"
# es wird überprüft welche Version von SNMP verwendet wird.
# auf die erfolgreichen IP Adressen werden nun SNMP Abfragen gemacht mittels SNMPWALK
# die Übergabe von Werten an dieses Skript erfolgt dynamisch => die Reihenfolge spielt keine Rolle nur der Name des Parameter muss übereinstimmen siehe "case" Anweisung weiter unten

############################
#INFO:
#Dies ist die start Datei
#Aufruf wie folgt:
#bash snmpScan.bash nameFromValue=valueFromValue IP=10.0.10.0 SUBNET=24 VERSION=v1 COMMUNITY=public BENUTZER=admin PASSWORD=password1! ...
#bash snmpScan.bash IP=10.0.10.0 SUBNET=24 VERSION=v1 COMMUNITYSTRING=public MODEL=.1.3.6.1.2.1.47.1.1.1.1.13.1001 SERIALNUMBER=.1.3.6.1.2.1.47.1.1.1.1.11.1001 HOSTNAME=.1.3.6.1.2.1.1.5.0 MAC=.1.3.6.1.2.1.2.2.1.6 imageVersion=.1.3.6.1.2.1.47.1.1.1.1.9 vlans=.1.3.6.1.2.1.4.20.1.2 ID=123
############################

#Start
###############################
x=1
for value in "$@"
do
	#echo $value
	name=${value:0:`expr index "$value" =`-1}
	value=${value:`expr index "$value" =`}
	#echo $value
	case "${name}"
	in
		"IP") 			ip=${value};;	#IP Adresse
		"SUBNET") 		sm=${value};;	#Subnetmaske
		"VERSION") 		vs=${value};;	#Version
		"COMMUNITYSTRING") 	cm=${value};;	#Community
		"USERNAME") 		be=${value};;	#Benutzer
		"PASSWORD") 		pw=${value};;	#Passwort
		"SECURITYLEVEL") 	sl=${value};;	#Sicherheitslevel noAuthNoPriv || authNoPriv || authPriv
		"HASH") 		av=${value};;	#Anmeldung Verschlüsselung (Benutzername und PW) MD5 || SHA
		"ENCRYPTION")		tv=${value};;	#Traffic Verschlüsselung AES || DES
		"PRIVATE KEY")		tk=${value};;	#Traffic Schlüssel privKey
		"MODEL") 		mnoid=${value};;	#Modell Name 	OID
		"SERIALNUMBER") 	snoid=${value};;	#Seriennummer	OID
		"HOSTNAME") 		hnoid=${value};;	#Hostname	OID
		"MAC") 			mcoid=${value};;	#MAC Adresse	OID
		"ID")			idJSON=${value} && idWork=${value};;	#für eine eindeutige JSON Datei
		*)  			name[$x]=${name} && oid[$x]=${value} &&	x=$(($x+1)) #eine Benutzerdefinierte OID Nummer
	esac
done

###############################
#Speicherung der erfolgreichen IP-Adressen falls die Datei existiert von einem altem Scan wird sie hier gelöscht
if [ -f successful_ping$idWork.txt ]
then
rm successful_ping$idWork.txt
fi
if [ -f "$idJSON"_$ip.json ]
then
rm "$idJSON"_$ip.json
fi
###############################
## Berechnung 1ste und letzte IP
rueckgabe=$(startEndeIPBereich $ip $sm)
ersterHost=${rueckgabe:0:`expr index "$rueckgabe" =`-1}
letzterHost=${rueckgabe:`expr index "$rueckgabe" =`}
###############################
startPingScan $ersterHost $letzterHost $idWork 
#####################################################################################################################################################################
#zum Testen in einer abgeschotteten Umgebung ohne Hosts
#echo "10.0.10.253" >>  successful_ping$idWork.txt
#echo "10.0.10.254" >>  successful_ping$idWork.txt
#####################################################################################################################################################################

########################
#für den NMAP Scan damit wir eine MAC zur IP haben (wird für die Diplomabgabe nicht mehr fertig implementiert)
#sudo nmap -sP -n 10.0.10.254
#
#
echo "fertig mit ping"
if [ -f successful_ping$idWork.txt ]
then
	echo "{\"$ip\": [" >> "$idJSON"_$ip.json				#Start Erzeugung der JSON Datei Objektname ist die Netz-ID
	###############################
	if [ "$be" == "" ]
	then
	firstRun="true"
		while read i
		do
			snmpReady=`snmpwalk -Ov -Oq -r 0 -$vs -c $cm -CE iso.3.6.1.2.1.1.2.0 $i` #überprüfen ob das Gerät SNMP unterstützt
			if [ "$snmpReady" == "" ]
			then
				echo kein SNMP fähiges Gerät
				if [ $firstRun == "true" ]
				then
					#IP Adresse:
					echo "{\"IP\": "\"$i\""," >> "$idJSON"_$ip.json
					firstRun="false"
					else
					sed -i -n -e :a -e '1,1!{P;N;D;};N;ba' "$idJSON"_$ip.json
					echo "}," >> "$idJSON"_$ip.json
					echo "{\"IP\": "\"$i\""," >> "$idJSON"_$ip.json
				fi
				echo "\"SUBNET\": "\"$sm\""," >> "$idJSON"_$ip.json
				echo "\"MODELNAME\": "\"noSNMPsupport\""," >> "$idJSON"_$ip.json
				echo "\"SERIALNUMBER\": "\"noSNMPsupport\""," >> "$idJSON"_$ip.json
				echo "\"MAC\": "\"noSNMPsupport\""," >> "$idJSON"_$ip.json
				echo "\"HOSTNAME\": "\"noSNMPsupport\""," >> "$idJSON"_$ip.json

				if [ $x -eq 1 ]
				then
					sed -i -n -e :a -e '1,1!{P;N;D;};N;ba' "$idJSON"_$ip.json
					echo "\"HOSTNAME\": "\"\""" >> "$idJSON"_$ip.json
					echo "}," >> "$idJSON"_$ip.json
				else
					for ((differentOID=1;differentOID<=$x;++differentOID))
					do
						if [ $(($x-1)) -eq $differentOID ]
						then
							echo "\"${name[$differentOID]}\": "\"noSNMPsupport\""" >> "$idJSON"_$ip.json
							echo >> "$idJSON"_$ip.json
							differentOID=$(($differentOID+1))
						else
							echo "\"${name[$differentOID]}\": "\"noSNMPsupport\""," >> "$idJSON"_$ip.json
							echo >> "$idJSON"_$ip.json
							sed -i -n -e :a -e '1,1!{P;N;D;};N;ba' "$idJSON"_$ip.json
						fi
					done
				fi
            ###############################
			else
                if [ $firstRun == "true" ]
                then
                    #IP Adresse:
                    echo "{\"IP\": "\"$i\""," >> "$idJSON"_$ip.json
                    firstRun="false"
                else
                    sed -i -n -e :a -e '1,1!{P;N;D;};N;ba' "$idJSON"_$ip.json
                    echo "}," >> "$idJSON"_$ip.json
                    echo "{\"IP\": "\"$i\""," >> "$idJSON"_$ip.json
                fi

                #Subnetmaske:
                echo "\"SUBNET\": "\"$sm\""," >> "$idJSON"_$ip.json


                #ausführen SNMP Scans für Version 1 und 2
                #Modell Name:
                mn=`snmpwalk -Ov -Oq -$vs -c $cm $i $mnoid` >/dev/null 2>&1
                firstValue=${mn:0:1}
                if [ $firstValue == "\"" ]
                then
                    :
                else
                    mn=\"$mn\"
                fi
                echo "\"MODELNAME\": "$mn"," >> "$idJSON"_$ip.json

                #Seriennummer:
                sn=`snmpwalk -Ov -Oq -$vs -c $cm $i $snoid` >/dev/null 2>&1
                firstValue=${sn:0:1}
                if [ $firstValue == "\"" ]
                then
                    :
                else
                    sn=\"$sn\"
                fi
                echo "\"SERIALNUMBER\": "$sn"," >> "$idJSON"_$ip.json


                #MAC Adressen von allen Ports:
                mc=`snmpwalk -Ov -Oq -Ob -$vs -c $cm $i $mcoid` >/dev/null 2>&1
                echo $mc > zwischenspeicher$idWork.txt
                stringfinal=
                mcStringfinalEnd=
                while read k
                do
                    mc=$k
                done < zwischenspeicher$idWork.txt
                IFS=':' read -r -a array <<< $mc
                for index in "${!array[@]}"
                do
                    stringfinal="$stringfinal${array[index]}"
                done
                IFS=' ' read -r -a array <<< "$stringfinal"
                for index in "${!array[@]}"
                do
                    if [ "${array[index]}" == "" ]
                    then
                        :
                    else
                        mcStringfinalEnd=$mcStringfinalEnd\"${array[index]}\",
                    fi
                done
                echo "\"MAC\": "\[${mcStringfinalEnd%\,*}\]"," >> "$idJSON"_$ip.json
                #############################################

                #HOST Name:
                hn=`snmpwalk -Ov -Oq -$vs -c $cm $i $hnoid` >/dev/null 2>&1
                firstValue=${hn:0:1}
                if [ $firstValue == "\"" ]
                then
                    :
                else
                    hn=\"$hn\"
                fi
                echo "\"HOSTNAME\": "$hn"," >> "$idJSON"_$ip.json
                if [ $x -eq 1 ]						#abfragen ob zusätzliche OIDs eingegeben wurden
                then
                    sed -i -n -e :a -e '1,1!{P;N;D;};N;ba' "$idJSON"_$ip.json
                    echo "\"HOSTNAME\": "$hn"" >> "$idJSON"_$ip.json
                    echo "}," >> "$idJSON"_$ip.json
                else
                    for ((differentOID=1;differentOID<=$x;++differentOID))
                    do
                        if [ $(($x-1)) -eq $differentOID ]
                        then
                            stringfinal=
                            oidValue[$differentOID]=`snmpwalk -Ov -Oq -$vs -c $cm $i ${oid[$differentOID]}` >/dev/null 2>&1
                            echo ${oidValue[$differentOID]} > zwischenspeicher$idWork.txt
                            while read k
                            do
                                oidValue[$differentOID]=$k
                            done < zwischenspeicher$idWork.txt
                            IFS=' ' read -r -a array <<< ${oidValue[$differentOID]}
                            stringfinal=
                            oidValuefinal[$differentOID]=
                            for index in "${!array[@]}"
                            do
                                firstValue=
                                firstValue=${array[index]:0:1}
                                if [ $firstValue == "\"" ]
                                then
                                    stringfinal="$stringfinal${array[index]}"
                                else
                                    stringfinal="$stringfinal\"${array[index]}\""
                                fi
                                    #stringfinal="$stringfinal${array[index]}"
                                #stringfinal=$stringfinal
                            done
                            IFS='" ' read -r -a array <<< "$stringfinal"
                            for index in "${!array[@]}"
                            do
                                if [ "${array[index]}" == "" ]
                                then
                                    :
                                else
                                    oidValuefinal[$differentOID]=${oidValuefinal[$differentOID]}\"${array[index]}\",
                                fi
                            done

                            echo "\"${name[$differentOID]}\": "\[${oidValuefinal[$differentOID]%\,*}\]"" >> "$idJSON"_$ip.json
                            echo >> "$idJSON"_$ip.json
                            differentOID=$(($differentOID+1))
                            #############################################
                        else
                            #############################################
                            stringfinal=
                            oidValuefinal[$differentOID]=
                            oidValue[$differentOID]=`snmpwalk -Ov -Oq -$vs -c $cm $i ${oid[$differentOID]}` >/dev/null 2>&1
                            echo ${oidValue[$differentOID]} > zwischenspeicher$idWork.txt
                            while read k
                            do
                                oidValue[$differentOID]=$k
                            done < zwischenspeicher$idWork.txt
                            IFS=' ' read -r -a array <<< ${oidValue[$differentOID]}
                            for index in "${!array[@]}"
                            do
								firstValue=
                                firstValue=${array[index]:0:1}
                                if [ $firstValue == "\"" ]
                                then
                                    stringfinal="$stringfinal${array[index]}"
                                else
                                    stringfinal="$stringfinal\"${array[index]}\""
                                fi

				#stringfinal=\"$stringfinal${array[index]}\"
                            done
                            IFS='"' read -r -a array <<< "$stringfinal"
                            for index in "${!array[@]}"
                            do
                                if [ "${array[index]}" == "" ]
                                then
                                    :
                                else
                                    oidValuefinal[$differentOID]=${oidValuefinal[$differentOID]}\"${array[index]}\",
				fi
                            done

                            echo "\"${name[$differentOID]}\": "\[${oidValuefinal[$differentOID]%\,*}\]"," >> "$idJSON"_$ip.json
                            echo >> "$idJSON"_$ip.json
                            sed -i -n -e :a -e '1,1!{P;N;D;};N;ba' "$idJSON"_$ip.json
                        fi
                    done
                fi
		    fi
		done < successful_ping$idWork.txt
	    sed -i -n -e :a -e '1,1!{P;N;D;};N;ba' "$idJSON"_$ip.json
	    echo "}]}" >> "$idJSON"_$ip.json
	#ausführen SNMP Scans für Version 3 ohne Traffic Verschlüsselung und ohne Passwort
	else if [ "$pw" == "" ]
	then
	    firstRun="true"
		while read i
		do
			snmpReady=`snmpwalk -Ov -Oq -r 0 -$vs -l $sl -u $be -CE iso.3.6.1.2.1.1.2.0 $i` #überprüfen ob das Gerät SNMP unterstützt
			if [ "$snmpReady" == "" ]
			then
				echo kein SNMP fähiges Gerät
				if [ $firstRun == "true" ]
				then
					#IP Adresse:
					echo "{\"IP\": "\"$i\""," >> "$idJSON"_$ip.json
					firstRun="false"
					else
					sed -i -n -e :a -e '1,1!{P;N;D;};N;ba' "$idJSON"_$ip.json
					echo "}," >> "$idJSON"_$ip.json
					echo "{\"IP\": "\"$i\""," >> "$idJSON"_$ip.json
				fi
				echo "\"SUBNET\": "\"$sm\""," >> "$idJSON"_$ip.json
				echo "\"MODELNAME\": "\"noSNMPsupport\""," >> "$idJSON"_$ip.json
				echo "\"SERIALNUMBER\": "\"noSNMPsupport\""," >> "$idJSON"_$ip.json
				echo "\"MAC\": "\"noSNMPsupport\""," >> "$idJSON"_$ip.json
				echo "\"HOSTNAME\": "\"noSNMPsupport\""," >> "$idJSON"_$ip.json

				if [ $x -eq 1 ]
				then
					sed -i -n -e :a -e '1,1!{P;N;D;};N;ba' "$idJSON"_$ip.json
					echo "\"HOSTNAME\": "\"\""" >> "$idJSON"_$ip.json
					echo "}," >> "$idJSON"_$ip.json
				else
					for ((differentOID=1;differentOID<=$x;++differentOID))
					do
						if [ $(($x-1)) -eq $differentOID ]
						then
							echo "\"${name[$differentOID]}\": "\"noSNMPsupport\""" >> "$idJSON"_$ip.json
							echo >> "$idJSON"_$ip.json
							differentOID=$(($differentOID+1))
						else
							echo "\"${name[$differentOID]}\": "\"noSNMPsupport\""," >> "$idJSON"_$ip.json
							echo >> "$idJSON"_$ip.json
							sed -i -n -e :a -e '1,1!{P;N;D;};N;ba' "$idJSON"_$ip.json
						fi
					done
				fi
            ###############################
			else
                if [ $firstRun == "true" ]
                then
                    #IP Adresse:
                    echo "{\"IP\": "\"$i\""," >> "$idJSON"_$ip.json
                    firstRun="false"
                else
                    sed -i -n -e :a -e '1,1!{P;N;D;};N;ba' "$idJSON"_$ip.json
                    echo "}," >> "$idJSON"_$ip.json
                    echo "{\"IP\": "\"$i\""," >> "$idJSON"_$ip.json
                fi

                #Subnetmaske:
                echo "\"SUBNET\": "\"$sm\""," >> "$idJSON"_$ip.json


                #ausführen SNMP Scans für Version 3
                #Modell Name:
                mn=`snmpwalk -Ov -Oq -$vs -l $sl -u $be $i $mnoid` >/dev/null 2>&1
                firstValue=${mn:0:1}
                if [ $firstValue == "\"" ]
                then
                    :
                else
                    mn=\"$mn\"
                fi
                echo "\"MODELNAME\": "$mn"," >> "$idJSON"_$ip.json

                #Seriennummer:
                sn=`snmpwalk -Ov -Oq -$vs -l $sl -u $be $i $snoid` >/dev/null 2>&1

                firstValue=${sn:0:1}
                if [ $firstValue == "\"" ]
                then
                    :
                else
                    sn=\"$sn\"
                fi
                echo "\"SERIALNUMBER\": "$sn"," >> "$idJSON"_$ip.json

                #MAC
                mc=`snmpwalk -Ov -Oq -$vs -l $sl -u $be $i $mcoid` >/dev/null 2>&1

                echo $mc > zwischenspeicher$idWork.txt
                stringfinal=
                mcStringfinalEnd=
                while read k
                do
                    mc=$k
                done < zwischenspeicher$idWork.txt
                IFS=':' read -r -a array <<< $mc
                for index in "${!array[@]}"
                do
                    stringfinal="$stringfinal${array[index]}"
                done
                IFS=' ' read -r -a array <<< "$stringfinal"
                for index in "${!array[@]}"
                do
                    if [ "${array[index]}" == "" ]
                    then
                        :
                    else
                        mcStringfinalEnd=$mcStringfinalEnd\"${array[index]}\",
                    fi
                done
                echo "\"MAC\": "\[${mcStringfinalEnd%\,*}\]"," >> "$idJSON"_$ip.json
                #############################################

                #HOST Name:
                hn=`snmpwalk -Ov -Oq -$vs -l $sl -u $be $i $hnoid` >/dev/null 2>&1

                firstValue=${hn:0:1}
                if [ $firstValue == "\"" ]
                then
                    :
                else
                    hn=\"$hn\"
                fi
                echo "\"HOSTNAME\": "$hn"," >> "$idJSON"_$ip.json

                if [ $x -eq 1 ]						#abfragen ob zusätzliche OIDs eingegeben wurden
                then
                    sed -i -n -e :a -e '1,1!{P;N;D;};N;ba' "$idJSON"_$ip.json
                    echo "\"HOSTNAME\": "$hn"" >> "$idJSON"_$ip.json
                    echo "}," >> "$idJSON"_$ip.json
                else
                    for ((differentOID=1;differentOID<=$x;++differentOID))
                    do
                        if [ $(($x-1)) -eq $differentOID ]
                        then
                            #############################################
                            stringfinal=
                            oidValue[$differentOID]=`snmpwalk -Ov -Oq -$vs -l $sl -u $be $i ${oid[$differentOID]}` >/dev/null 2>&1
                            echo ${oidValue[$differentOID]} > zwischenspeicher$idWork.txt
                            while read k
                            do
                                oidValue[$differentOID]=$k
                            done < zwischenspeicher$idWork.txt
                            IFS=' ' read -r -a array <<< ${oidValue[$differentOID]}
                            stringfinal=
                            oidValuefinal[$differentOID]=
                            for index in "${!array[@]}"
                            do
                                firstValue=
                                firstValue=${array[index]:0:1}
                                if [ $firstValue == "\"" ]
                                then
                                    stringfinal="$stringfinal${array[index]}"
                                else
                                    stringfinal="$stringfinal\"${array[index]}\""
                                fi
                            done
                            IFS='" ' read -r -a array <<< "$stringfinal"
                            for index in "${!array[@]}"
                            do
                                if [ "${array[index]}" == "" ]
                                then
                                    :
                                else
                                    oidValuefinal[$differentOID]=${oidValuefinal[$differentOID]}\"${array[index]}\",
                                fi
                            done
                            echo "\"${name[$differentOID]}\": "\[${oidValuefinal[$differentOID]%\,*}\]"" >> "$idJSON"_$ip.json
                            echo >> "$idJSON"_$ip.json
                            differentOID=$(($differentOID+1))
                            #############################################
                        else
                            #############################################
                            stringfinal=
                            oidValuefinal[$differentOID]=
                            oidValue[$differentOID]=`snmpwalk -Ov -Oq -$vs -l $sl -u $be $i ${oid[$differentOID]}` >/dev/null 2>&1

                            echo ${oidValue[$differentOID]} > zwischenspeicher$idWork.txt
                            while read k
                            do
                                oidValue[$differentOID]=$k
                            done < zwischenspeicher$idWork.txt
                            IFS=' ' read -r -a array <<< ${oidValue[$differentOID]}
                            for index in "${!array[@]}"
                            do
								firstValue=
                                firstValue=${array[index]:0:1}
                                if [ $firstValue == "\"" ]
                                then
                                    stringfinal="$stringfinal${array[index]}"
                                else
                                    stringfinal="$stringfinal\"${array[index]}\""
                                fi

                                #stringfinal="$stringfinal${array[index]}"
                            done
                            IFS='"' read -r -a array <<< "$stringfinal"
                            for index in "${!array[@]}"
                            do
                                if [ "${array[index]}" == "" ]
                                then
                                    :
                                else
                                    oidValuefinal[$differentOID]=${oidValuefinal[$differentOID]}\"${array[index]}\"
                                fi
                            done
                            echo "\"${name[$differentOID]}\": "\[${oidValuefinal[$differentOID]%\,*}\]"," >> "$idJSON"_$ip.json
                            echo >> "$idJSON"_$ip.json
                            #############################################
                            #echo "\"${name[$differentOID]}\": "${oidValue[$differentOID]}"," >> "$idJSON"_$ip.json
                            #echo >> "$idJSON"_$ip.json			#frag mich nicht wieso dies so sein muss => bei den Standardwerten funktioniert die "richtige Variante"
                            sed -i -n -e :a -e '1,1!{P;N;D;};N;ba' "$idJSON"_$ip.json
                        fi
                    done
                fi
		    fi
		done < successful_ping$idWork.txt
		sed -i -n -e :a -e '1,1!{P;N;D;};N;ba' "$idJSON"_$ip.json
		echo "}]}" >> "$idJSON"_$ip.json
##################################################################################################################################################################################################################
	else if [ "$tv" == "" ]
	then
		firstRun="true"
		while read i
		do
			snmpReady=`snmpwalk -Ov -Oq -r 0 -$vs -l $sl -u $be -a $av -A $pw -CE iso.3.6.1.2.1.1.2.0 $i` #überprüfen ob das Gerät SNMP unterstützt
			if [ "$snmpReady" == "" ]
			then
				echo kein SNMP fähiges Gerät
				if [ $firstRun == "true" ]
				then
					#IP Adresse:
					echo "{\"IP\": "\"$i\""," >> "$idJSON"_$ip.json
					firstRun="false"
					else
					sed -i -n -e :a -e '1,1!{P;N;D;};N;ba' "$idJSON"_$ip.json
					echo "}," >> "$idJSON"_$ip.json
					echo "{\"IP\": "\"$i\""," >> "$idJSON"_$ip.json
				fi
				echo "\"SUBNET\": "\"$sm\""," >> "$idJSON"_$ip.json
				echo "\"MODELNAME\": "\"noSNMPsupport\""," >> "$idJSON"_$ip.json
				echo "\"SERIALNUMBER\": "\"noSNMPsupport\""," >> "$idJSON"_$ip.json
				echo "\"MAC\": "\"noSNMPsupport\""," >> "$idJSON"_$ip.json
				echo "\"HOSTNAME\": "\"noSNMPsupport\""," >> "$idJSON"_$ip.json

				if [ $x -eq 1 ]
				then
					sed -i -n -e :a -e '1,1!{P;N;D;};N;ba' "$idJSON"_$ip.json
					echo "\"HOSTNAME\": "\"\""" >> "$idJSON"_$ip.json
					echo "}," >> "$idJSON"_$ip.json
				else
					for ((differentOID=1;differentOID<=$x;++differentOID))
					do
						if [ $(($x-1)) -eq $differentOID ]
						then
							echo "\"${name[$differentOID]}\": "\"noSNMPsupport\""" >> "$idJSON"_$ip.json
							echo >> "$idJSON"_$ip.json
							differentOID=$(($differentOID+1))
						else
							echo "\"${name[$differentOID]}\": "\"noSNMPsupport\""," >> "$idJSON"_$ip.json
							echo >> "$idJSON"_$ip.json
							sed -i -n -e :a -e '1,1!{P;N;D;};N;ba' "$idJSON"_$ip.json
						fi
					done
				fi
            ###############################
			else
                if [ $firstRun == "true" ]
                then
                    #IP Adresse:
                    echo "{\"IP\": "\"$i\""," >> "$idJSON"_$ip.json
                    firstRun="false"
                else
                    sed -i -n -e :a -e '1,1!{P;N;D;};N;ba' "$idJSON"_$ip.json
                    echo "}," >> "$idJSON"_$ip.json
                    echo "{\"IP\": "\"$i\""," >> "$idJSON"_$ip.json
                fi

                #Subnetmaske:
                echo "\"SUBNET\": "\"$sm\""," >> "$idJSON"_$ip.json


                #ausführen SNMP Scans für Version 3
                #Modell Name:
                mn=`snmpwalk -Ov -Oq -$vs -l $sl -u $be -a $av -A $pw $i $mnoid` >/dev/null 2>&1

                firstValue=${mn:0:1}
                if [ $firstValue == "\"" ]
                then
                    :
                else
                    mn=\"$mn\"
                fi
                echo "\"MODELNAME\": "$mn"," >> "$idJSON"_$ip.json

                #Seriennummer:
                sn=`snmpwalk -Ov -Oq -$vs -l $sl -u $be -a $av -A $pw $i $snoid` >/dev/null 2>&1


                firstValue=${sn:0:1}
                if [ $firstValue == "\"" ]
                then
                    :
                else
                    sn=\"$sn\"
                fi
                echo "\"SERIALNUMBER\": "$sn"," >> "$idJSON"_$ip.json

                #MAC
                mc=`snmpwalk -Ov -Oq -$vs -l $sl -u $be -a $av -A $pw $i $mcoid` >/dev/null 2>&1

                echo $mc > zwischenspeicher$idWork.txt
                stringfinal=
                mcStringfinalEnd=
                while read k
                do
                    mc=$k
                done < zwischenspeicher$idWork.txt
                IFS=':' read -r -a array <<< $mc
                for index in "${!array[@]}"
                do
                    stringfinal="$stringfinal${array[index]}"
                done
                IFS=' ' read -r -a array <<< "$stringfinal"
                for index in "${!array[@]}"
                do
                    if [ "${array[index]}" == "" ]
                    then
                        :
                    else
                        mcStringfinalEnd=$mcStringfinalEnd\"${array[index]}\",
                    fi
                done
                echo "\"MAC\": "\[${mcStringfinalEnd%\,*}\]"," >> "$idJSON"_$ip.json
                #############################################

                #HOST Name:
                #snmpwalk -v3 -l noAuthNoPriv -u <SNMP-User> <IP-Adresse> 1.3.6.1.4.1.6574.2.1.1.5
                hn=`snmpwalk -Ov -Oq -$vs -l $sl -u $be -a $av -A $pw $i $hnoid` >/dev/null 2>&1

                firstValue=${hn:0:1}
                if [ $firstValue == "\"" ]
                then
                    :
                else
                    hn=\"$hn\"
                fi
                echo "\"HOSTNAME\": "$hn"," >> "$idJSON"_$ip.json

                if [ $x -eq 1 ]						#abfragen ob zusätzliche OIDs eingegeben wurden
                then
                    sed -i -n -e :a -e '1,1!{P;N;D;};N;ba' "$idJSON"_$ip.json
                    echo "\"HOSTNAME\": "$hn"" >> "$idJSON"_$ip.json
                    echo "}," >> "$idJSON"_$ip.json
                else
                    for ((differentOID=1;differentOID<=$x;++differentOID))
                    do
                        if [ $(($x-1)) -eq $differentOID ]
                        then
                            #############################################
                            stringfinal=
                            #differentOID=$(($differentOID-1))
                            oidValue[$differentOID]=`snmpwalk -Ov -Oq -$vs -l $sl -u $be -a $av -A $pw $i ${oid[$differentOID]}` >/dev/null 2>&1
                            echo ${oidValue[$differentOID]} > zwischenspeicher$idWork.txt
                            while read k
                            do
                                oidValue[$differentOID]=$k
                            done < zwischenspeicher$idWork.txt
                            IFS=' ' read -r -a array <<< ${oidValue[$differentOID]}
                            stringfinal=
                            oidValuefinal[$differentOID]=
                            for index in "${!array[@]}"
                            do
                                firstValue=
                                firstValue=${array[index]:0:1}
                                if [ $firstValue == "\"" ]
                                then
                                    stringfinal="$stringfinal${array[index]}"
                                else
                                    stringfinal="$stringfinal\"${array[index]}\""
                                fi
                            done
                            IFS='" ' read -r -a array <<< "$stringfinal"
                            for index in "${!array[@]}"
                            do
                                if [ "${array[index]}" == "" ]
                                then
                                    :
                                else
                                    oidValuefinal[$differentOID]=${oidValuefinal[$differentOID]}\"${array[index]}\",
                                fi
                            done
                            echo "\"${name[$differentOID]}\": "\[${oidValuefinal[$differentOID]%\,*}\]"" >> "$idJSON"_$ip.json
                            echo >> "$idJSON"_$ip.json
                            differentOID=$(($differentOID+1))
                            #############################################
                        else
                            #############################################
                            stringfinal=
                            oidValuefinal[$differentOID]=
                            oidValue[$differentOID]=`snmpwalk -Ov -Oq -$vs -l $sl -u $be -a $av -A $pw $i ${oid[$differentOID]}` >/dev/null 2>&1

                            echo ${oidValue[$differentOID]} > zwischenspeicher$idWork.txt
                            while read k
                            do
                                oidValue[$differentOID]=$k
                            done < zwischenspeicher$idWork.txt
                            IFS=' ' read -r -a array <<< ${oidValue[$differentOID]}
                            for index in "${!array[@]}"
                            do
								firstValue=
                                firstValue=${array[index]:0:1}
                                if [ $firstValue == "\"" ]
                                then
                                    stringfinal="$stringfinal${array[index]}"
                                else
                                    stringfinal="$stringfinal\"${array[index]}\""
                                fi

                                #stringfinal="$stringfinal${array[index]}"
                            done
                            IFS='"' read -r -a array <<< "$stringfinal"
                            for index in "${!array[@]}"
                            do
                                if [ "${array[index]}" == "" ]
                                then
                                    :
                                else
                                    oidValuefinal[$differentOID]=${oidValuefinal[$differentOID]}\"${array[index]}\"
                                fi
                            done
                            echo "\"${name[$differentOID]}\": "\[${oidValuefinal[$differentOID]%\,*}\]"," >> "$idJSON"_$ip.json
                            echo >> "$idJSON"_$ip.json
                            #############################################
                            sed -i -n -e :a -e '1,1!{P;N;D;};N;ba' "$idJSON"_$ip.json
                        fi
                    done
                fi
		    fi
		done < successful_ping$idWork.txt
		sed -i -n -e :a -e '1,1!{P;N;D;};N;ba' "$idJSON"_$ip.json
		echo "}]}" >> "$idJSON"_$ip.json

##################################################################################################################################################################################################################
	else
	#ausführen SNMP Scans für Version 3 mit Traffic Verschlüsselung
		firstRun="true"
		while read i
		do
			snmpReady=`snmpwalk -Ov -Oq -r 0 -$vs -l $sl -u $be -a $av -A $pw -x $tv -X $kv -CE iso.3.6.1.2.1.1.2.0 $i` #überprüfen ob das Gerät SNMP unterstützt
			if [ "$snmpReady" == "" ]
			then
				echo kein SNMP fähiges Gerät
				if [ $firstRun == "true" ]
				then
					#IP Adresse:
					echo "{\"IP\": "\"$i\""," >> "$idJSON"_$ip.json
					firstRun="false"
					else
					sed -i -n -e :a -e '1,1!{P;N;D;};N;ba' "$idJSON"_$ip.json
					echo "}," >> "$idJSON"_$ip.json
					echo "{\"IP\": "\"$i\""," >> "$idJSON"_$ip.json
				fi
				echo "\"SUBNET\": "\"$sm\""," >> "$idJSON"_$ip.json
				echo "\"MODELNAME\": "\"noSNMPsupport\""," >> "$idJSON"_$ip.json
				echo "\"SERIALNUMBER\": "\"noSNMPsupport\""," >> "$idJSON"_$ip.json
				echo "\"MAC\": "\"noSNMPsupport\""," >> "$idJSON"_$ip.json
				echo "\"HOSTNAME\": "\"noSNMPsupport\""," >> "$idJSON"_$ip.json

				if [ $x -eq 1 ]
				then
					sed -i -n -e :a -e '1,1!{P;N;D;};N;ba' "$idJSON"_$ip.json
					echo "\"HOSTNAME\": "\"\""" >> "$idJSON"_$ip.json
					echo "}," >> "$idJSON"_$ip.json
				else
					for ((differentOID=1;differentOID<=$x;++differentOID))
					do
						if [ $(($x-1)) -eq $differentOID ]
						then
							echo "\"${name[$differentOID]}\": "\"noSNMPsupport\""" >> "$idJSON"_$ip.json
							echo >> "$idJSON"_$ip.json
							differentOID=$(($differentOID+1))
						else
							echo "\"${name[$differentOID]}\": "\"noSNMPsupport\""," >> "$idJSON"_$ip.json
							echo >> "$idJSON"_$ip.json
							sed -i -n -e :a -e '1,1!{P;N;D;};N;ba' "$idJSON"_$ip.json
						fi
					done
				fi
            ###############################
			else
                if [ $firstRun == "true" ]
                then
                    #IP Adresse:
                    echo "{\"IP\": "\"$i\""," >> "$idJSON"_$ip.json
                    firstRun="false"
                else
                    sed -i -n -e :a -e '1,1!{P;N;D;};N;ba' "$idJSON"_$ip.json
                    echo "}," >> "$idJSON"_$ip.json
                    echo "{\"IP\": "\"$i\""," >> "$idJSON"_$ip.json
                fi

                #Subnetmaske:
                echo "\"SUBNET\": "\"$sm\""," >> "$idJSON"_$ip.json


                #ausführen SNMP Scans für Version 3
                #Modell Name:
                mn=`snmpwalk -Ov -Oq -$vs -l $sl -u $be -a $av -A $pw -x $tv -X $kv $i $mnoid` >/dev/null 2>&1

                firstValue=${mn:0:1}
                if [ $firstValue == "\"" ]
                then
                    :
                else
                    mn=\"$mn\"
                fi
                echo "\"MODELNAME\": "$mn"," >> "$idJSON"_$ip.json

                #Seriennummer:
                sn=`snmpwalk -Ov -Oq -$vs -l $sl -u $be -a $av -A $pw -x $tv -X $kv $i $snoid` >/dev/null 2>&1


                firstValue=${sn:0:1}
                if [ $firstValue == "\"" ]
                then
                    :
                else
                    sn=\"$sn\"
                fi
                echo "\"SERIALNUMBER\": "$sn"," >> "$idJSON"_$ip.json

                #MAC
                mc=`snmpwalk -Ov -Oq -$vs -l $sl -u $be -a $av -A $pw -x $tv -X $kv $i $mcoid` >/dev/null 2>&1

                echo $mc > zwischenspeicher$idWork.txt
                stringfinal=
                mcStringfinalEnd=
                while read k
                do
                    mc=$k
                done < zwischenspeicher$idWork.txt
                IFS=':' read -r -a array <<< $mc
                for index in "${!array[@]}"
                do
                    stringfinal="$stringfinal${array[index]}"
                done
                IFS=' ' read -r -a array <<< "$stringfinal"
                for index in "${!array[@]}"
                do
                    if [ "${array[index]}" == "" ]
                    then
                        :
                    else
                        mcStringfinalEnd=$mcStringfinalEnd\"${array[index]}\",
                    fi
                done
                echo "\"MAC\": "\[${mcStringfinalEnd%\,*}\]"," >> "$idJSON"_$ip.json
                #############################################

                #HOST Name:
                hn=`snmpwalk -Ov -Oq -$vs -l $sl -u $be -a $av -A $pw -x $tv -X $kv $i $hnoid` >/dev/null 2>&1

                firstValue=${hn:0:1}
                if [ $firstValue == "\"" ]
                then
                    :
                else
                    hn=\"$hn\"
                fi
                echo "\"HOSTNAME\": "$hn"," >> "$idJSON"_$ip.json

                if [ $x -eq 1 ]						#abfragen ob zusätzliche OIDs eingegeben wurden
                then
                    sed -i -n -e :a -e '1,1!{P;N;D;};N;ba' "$idJSON"_$ip.json
                    echo "\"HOSTNAME\": "$hn"" >> "$idJSON"_$ip.json
                    echo "}," >> "$idJSON"_$ip.json
                else
                    for ((differentOID=1;differentOID<=$x;++differentOID))
                    do
                        if [ $(($x-1)) -eq $differentOID ]
                        then
                            #############################################
                            stringfinal=
                            #differentOID=$(($differentOID-1))
                            oidValue[$differentOID]=`snmpwalk -Ov -Oq -$vs -l $sl -u $be -a $av -A $pw -x $tv -X $kv $i ${oid[$differentOID]}` >/dev/null 2>&1
                            echo ${oidValue[$differentOID]} > zwischenspeicher$idWork.txt
                            while read k
                            do
                                oidValue[$differentOID]=$k
                            done < zwischenspeicher$idWork.txt
                            IFS=' ' read -r -a array <<< ${oidValue[$differentOID]}
                            stringfinal=
                            oidValuefinal[$differentOID]=
                            for index in "${!array[@]}"
                            do
                                firstValue=
                                firstValue=${array[index]:0:1}
                                if [ $firstValue == "\"" ]
                                then
                                    stringfinal="$stringfinal${array[index]}"
                                else
                                    stringfinal="$stringfinal\"${array[index]}\""
                                fi
                            done
                            IFS='" ' read -r -a array <<< "$stringfinal"
                            for index in "${!array[@]}"
                            do
                                if [ "${array[index]}" == "" ]
                                then
                                    :
                                else
                                    oidValuefinal[$differentOID]=${oidValuefinal[$differentOID]}\"${array[index]}\",
                                fi
                            done
                            echo "\"${name[$differentOID]}\": "\[${oidValuefinal[$differentOID]%\,*}\]"" >> "$idJSON"_$ip.json
                            echo >> "$idJSON"_$ip.json
                            differentOID=$(($differentOID+1))
                            #############################################
                        else
                            #############################################
                            stringfinal=
                            oidValuefinal[$differentOID]=
                            oidValue[$differentOID]=`snmpwalk -Ov -Oq -$vs -l $sl -u $be -a $av -A $pw -x $tv -X $kv $i ${oid[$differentOID]}` >/dev/null 2>&1

                            echo ${oidValue[$differentOID]} > zwischenspeicher$idWork.txt
                            while read k
                            do
                                oidValue[$differentOID]=$k
                            done < zwischenspeicher$idWork.txt
                            IFS=' ' read -r -a array <<< ${oidValue[$differentOID]}
                            for index in "${!array[@]}"
                            do
								firstValue=
                                firstValue=${array[index]:0:1}
                                if [ $firstValue == "\"" ]
                                then
                                    stringfinal="$stringfinal${array[index]}"
                                else
                                    stringfinal="$stringfinal\"${array[index]}\""
                                fi

                                #stringfinal="$stringfinal${array[index]}"
                            done
                            IFS='"' read -r -a array <<< "$stringfinal"
                            for index in "${!array[@]}"
                            do
                                if [ "${array[index]}" == "" ]
                                then
                                    :
                                else
                                    oidValuefinal[$differentOID]=${oidValuefinal[$differentOID]}\"${array[index]}\"
                                fi
                            done
                            echo "\"${name[$differentOID]}\": "\[${oidValuefinal[$differentOID]%\,*}\]"," >> "$idJSON"_$ip.json
                            echo >> "$idJSON"_$ip.json
                            #############################################
                            sed -i -n -e :a -e '1,1!{P;N;D;};N;ba' "$idJSON"_$ip.json
                        fi
                    done
    			fi
	    	fi
		done < successful_ping$idWork.txt
		sed -i -n -e :a -e '1,1!{P;N;D;};N;ba' "$idJSON"_$ip.json
		echo "}]}" >> "$idJSON"_$ip.json

##################################################################################################################################################################################################################
	fi
	fi
	fi
else
echo "Es wurden keine Hosts gefunden gefunden. Versichere dich, dass Hosts in deinem Netzwerk erreichbar sind. Zum Testen des Skriptes, in einer Umgebung wo keine Hosts erreichbar sind, kommentiere folgende 2 Zeilen nach der Zeile #zum Testen in einer abgeschotteten Umgebung ohne Hosts# aus. Kommentiere folgende Zeile ein# startPingScan $ ersterHost $ letzterHost $ idWork rueckgabe=$(startEndeIPBereich $ ip $ sm) "

fi
if [ -f successful_ping$idWork.txt ]
then
rm successful_ping$idWork.txt
fi
if [ -f zwischenspeicher$idWork.txt ]
then
rm zwischenspeicher$idWork.txt
fi
