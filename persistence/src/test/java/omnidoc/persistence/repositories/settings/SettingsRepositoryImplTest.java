package omnidoc.persistence.repositories.settings;

import junit.framework.TestCase;

import java.util.List;

public class SettingsRepositoryImplTest extends TestCase {

    private SettingsRepositoryImpl settingsRepository;

    @Override
    public void setUp() throws Exception {
        super.setUp();
        this.settingsRepository = new SettingsRepositoryImpl();
    }

    public void testPreferencesNotNull() {
        List<String> panelNames = settingsRepository.getPanelNamesBySiteId(1L);
        System.out.println(panelNames);
        assertNotNull(panelNames);
    }

    public void testPreferencesEmpty() {
        List<String> panelNames = settingsRepository.getPanelNamesBySiteId(1L);
        assertTrue(panelNames.isEmpty());
    }

    public void testAddSinglePreference() {
        settingsRepository.addPreference(1L, "first");
        List<String> panelNames = settingsRepository.getPanelNamesBySiteId(1L);
        assertEquals("first", panelNames.get(0));
    }

    public void testAddMultipleSinglePreference() {
        settingsRepository.addPreference(1L, "first");
        settingsRepository.addPreference(1L, "second");
        settingsRepository.addPreference(1L, "third");
        List<String> panelNames = settingsRepository.getPanelNamesBySiteId(1L);
        assertEquals("first", panelNames.get(0));
        assertEquals("second", panelNames.get(1));
        assertEquals("third", panelNames.get(2));
    }

    public void testRemoveOnEmptyPreferences() {
        settingsRepository.removePreference(1L, "first");
        List<String> panelNames = settingsRepository.getPanelNamesBySiteId(1L);
        assertTrue(panelNames.isEmpty());
    }

    public void testRemoveSinglePreference() {
        settingsRepository.addPreference(1L, "first");
        settingsRepository.removePreference(1L, "first");
        List<String> panelNames = settingsRepository.getPanelNamesBySiteId(1L);
        assertTrue(panelNames.isEmpty());
    }

    public void testRemoveMultipleSinglePreference() {
        settingsRepository.addPreference(1L, "first");
        settingsRepository.addPreference(1L, "second");
        settingsRepository.addPreference(1L, "third");
        settingsRepository.removePreference(1L, "second");
        List<String> panelNames = settingsRepository.getPanelNamesBySiteId(1L);
        assertEquals("first", panelNames.get(0));
        assertEquals("third", panelNames.get(1));
    }
}