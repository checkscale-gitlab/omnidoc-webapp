package omnidoc.persistence.repositories.settings;

import omnidoc.business.repositories.SettingsRepository;

import java.util.*;

public class SettingsRepositoryImpl implements SettingsRepository {

    private Map<Long, List<String>> panelNames = new HashMap<>();

    @Override
    public List<String> getPanelNamesBySiteId(Long siteId) {
        return panelNames.getOrDefault(siteId, new ArrayList<>());
    }

    @Override
    public void addPreference(Long siteId, String panelName) {
        panelNames.computeIfAbsent(
                siteId,
                k -> new ArrayList<>()
        ).add(panelName);
    }

    @Override
    public void removePreference(Long siteId, String panelName) {
        panelNames.getOrDefault(siteId, new ArrayList<>()).remove(panelName);
    }
}