package omnidoc.persistence.repositories.components;

import omnidoc.business.domain.topology.NetworkComponent;
import omnidoc.business.repositories.ComponentRepository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

public class ComponentRepositoryDbImpl implements ComponentRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<NetworkComponent> findAll() {
        TypedQuery<NetworkComponent> query = entityManager.createNamedQuery(NetworkComponent.findAll, NetworkComponent.class);
        return query.getResultList();
    }

    @Override
    public NetworkComponent findById(Long id) {
        return entityManager.find(NetworkComponent.class, id);
    }

    @Override
    public NetworkComponent save(NetworkComponent component) {
        entityManager.persist(component);
        return component;
    }

    @Override
    public void update(NetworkComponent component) {
        entityManager.merge(component);
    }

    @Override
    public void remove(NetworkComponent component) {
        component = entityManager.contains(component) ? component : entityManager.merge(component);
        entityManager.remove(component);
    }
}
