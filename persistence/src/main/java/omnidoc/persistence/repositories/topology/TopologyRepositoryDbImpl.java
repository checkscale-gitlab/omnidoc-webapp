package omnidoc.persistence.repositories.topology;

import omnidoc.business.domain.topology.NetworkTopology;
import omnidoc.business.repositories.ComponentRepository;
import omnidoc.business.repositories.TopologyRepository;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

public class TopologyRepositoryDbImpl implements TopologyRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Inject
    private ComponentRepository componentRepository;

    @Override
    public List<NetworkTopology> findAll() {
        TypedQuery<NetworkTopology> query = entityManager.createNamedQuery(NetworkTopology.findAll, NetworkTopology.class);
        return query.getResultList();
    }

    @Override
    public NetworkTopology findById(Long id) {
        return entityManager.find(NetworkTopology.class, id);
    }

    @Override
    public NetworkTopology save(NetworkTopology topology) {
        entityManager.persist(topology);
        return topology;
    }

    @Override
    public void update(NetworkTopology topology) {
        entityManager.merge(topology);
    }

    @Override
    public void remove(NetworkTopology topology) {
        topology = entityManager.contains(topology) ? topology : entityManager.merge(topology);

        topology.getComponents().forEach(component -> {
            componentRepository.remove(component);
        });
        topology.getComponents().clear();

        topology.getConnections().forEach(connection -> {
            connection.setSource(null);
            connection.setTarget(null);
            entityManager.remove(connection);
        });
        topology.getConnections().clear();

        entityManager.remove(topology);
    }
}
