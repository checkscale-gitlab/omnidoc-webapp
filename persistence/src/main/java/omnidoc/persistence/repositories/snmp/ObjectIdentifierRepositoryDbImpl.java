package omnidoc.persistence.repositories.snmp;

import omnidoc.business.domain.property.snmp.ObjectIdentifier;
import omnidoc.business.repositories.ObjectIdentifierRepository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

public class ObjectIdentifierRepositoryDbImpl implements ObjectIdentifierRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<ObjectIdentifier> findAll() {
        TypedQuery<ObjectIdentifier> query = entityManager.createNamedQuery(ObjectIdentifier.findAll, ObjectIdentifier.class);
        return query.getResultList();
    }

    @Override
    public ObjectIdentifier findById(Long oidId) {
        return entityManager.find(ObjectIdentifier.class, oidId);
    }

    @Override
    public ObjectIdentifier getByOid(String objectIdentifier) {
        TypedQuery<ObjectIdentifier> query = entityManager.createNamedQuery(ObjectIdentifier.findByOid, ObjectIdentifier.class);
        query.setParameter("oid", objectIdentifier);
        List<ObjectIdentifier> objectIdentifiers = query.getResultList();
        return objectIdentifiers.isEmpty() ? null : objectIdentifiers.get(0);
    }

    @Override
    public ObjectIdentifier getByName(String name) {
        TypedQuery<ObjectIdentifier> query = entityManager.createNamedQuery(ObjectIdentifier.findByName, ObjectIdentifier.class);
        query.setParameter("name", name);
        List<ObjectIdentifier> objectIdentifiers = query.getResultList();
        return objectIdentifiers.isEmpty() ? null : objectIdentifiers.get(0);
    }

    @Override
    public ObjectIdentifier save(ObjectIdentifier oid) {
        entityManager.persist(oid);
        return oid;
    }

    @Override
    public void update(ObjectIdentifier oid) {
        entityManager.merge(oid);
    }

    @Override
    public void remove(ObjectIdentifier oid) {
        entityManager.remove(entityManager.merge(oid));
    }
}
